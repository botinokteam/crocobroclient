var prevX = -1;
var prevY = -1;



function draw(mouseX, mouseY, send)
{    
    if (prevX == -1 ) prevX = mouseX;
    if (prevY == -1 ) prevY = mouseY;

    var ctx = mainCanvas.getContext('2d');
    ctx.fillStyle = Qt.rgba(1, 0, 0, 1);
    ctx.lineWidth = 1;
    ctx.strokeStyle = '#ff0000';

    ctx.moveTo(prevX, prevY);
    ctx.lineTo(mouseX, mouseY);
    ctx.stroke();
    mainCanvas.requestPaint();

    prevX = mouseX;
    prevY = mouseY;

    if (send === true)
        Client.sendDatagram(mouseX, mouseY);
}


function clearState()
{
    prevX = -1;
    prevY = -1;
}


function clearCanvas()
{
    var ctx = mainCanvas.getContext('2d');
    ctx.fillStyle = Qt.rgba(1, 1, 1, 1);
    ctx.fillRect(0, 0, mainCanvas.width, mainCanvas.height);
    mainCanvas.requestPaint();
    ctx.reset();

    clearState();
}





function showMainForm()
{
    if ( !loginTf.text )
        return;
    if ( !portTf.text )
        return;


    loginWindow.hide();    
    var component  = Qt.createComponent("MainWindow.qml");
    var win = component.createObject(loginWindow);
    win.showWnd(loginTf.text, portTf.text);
}


function logIn(login, port)
{
    var res = Client.logIn(login, port);

    if ( res )
        statusLabel.text = "connected!";
    else
        statusLabel.text = "failed!!!";
}
