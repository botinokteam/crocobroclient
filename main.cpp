#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "Backend/client.h"
#include "Backend/server.h"


static QObject* backendClientProvider(QQmlEngine* engine, QJSEngine* scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    auto client = new Client();
    return client;
}



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qmlRegisterType<Client>("CrocoBro.Client", 1, 0, "Client");
    qmlRegisterType<Server>("CrocoBro.Server", 1, 0, "Server");
    qmlRegisterSingletonType<Client>("Backend.Client", 1, 0, "Client", backendClientProvider);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
