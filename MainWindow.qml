import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

import Backend.Client 1.0
import "Logic.js" as Logic


ApplicationWindow
{
    id: mainWnd

    visible: true
    width: 640
    height: 480
    title: qsTr("CrocoClientGui")

    function showWnd(login, port)
    {
        Logic.logIn(login, port);
    }



    ColumnLayout
    {
        anchors.fill: parent
        spacing: 0

        Rectangle
        {
            height: 30
            Layout.fillWidth: true
            color: "gray"

            RowLayout
            {
                anchors.fill: parent
                anchors.margins: 5

                Label
                {
                    Layout.alignment: Qt.AlignVCenter
                    text: "Status: "
                }
                Label
                {
                    id: statusLabel
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    Layout.fillWidth: true

                    text: "connecting..."
                    color: "white"
                }
                Button
                {
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignRight

                    text: "Log In"
                    onClicked: Logic.logIn()
                }
                Button
                {
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignRight

                    text: "Clear"
                    onClicked: Logic.clearCanvas()
                }
            }
        }

        Canvas
        {
            id: mainCanvas
            Layout.fillWidth: true
            Layout.fillHeight: true


            //Client { id: crocoClient }
//            Server {
//                onDatagramReceived: { console.log("123"); Logic.draw(x, y, false); }
//            }
            MouseArea
            {
                id: canvasMouseArea

                //hoverEnabled: true
                anchors.fill: parent
                onPositionChanged: {
                    Logic.draw(mouseX, mouseY, true);
                }
                onReleased: { Logic.clearState(); }
            }
        }
    }


}
