import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "Logic.js" as Logic
import Backend.Client 1.0



ApplicationWindow
{
    id: loginWindow

    visible: true
    modality: Qt.ApplicationModal

    width: 500
    height: 100
    title: qsTr("CrocoBro: LoginWindow")




    GridLayout
    {
        columns: 3
        rows: 2

        anchors.leftMargin: 5
        anchors.topMargin: 5
        anchors.rightMargin: 5

        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right



        //----------- row 1
        Label { text: "Enter nickname: " }
        TextField
        {
            id: loginTf

            Layout.fillWidth: true
            Layout.columnSpan: 2
            width: 200

            text: "ilyanky"
        }



        //----------- row 2
        Label { text: "Enter port: " }
        TextField
        {
            id: portTf

            Layout.fillWidth: true
            width: 200

            text: "1455"
        }
        Button
        {
            id: acceptButton
            text: "Ok"

            onClicked: Logic.showMainForm();
        }
    }

}
