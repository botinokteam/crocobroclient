#include "client.h"



Client::Client(QObject *parent) : QObject(parent)
{    
}



void Client::sendDatagram(const float& x, const float& y)
{
    auto arr = QByteArray(reinterpret_cast<const char*>(&x), 4)
            + QByteArray(reinterpret_cast<const char*>(&y), 4);

    QNetworkDatagram datagram;
    datagram.setData(arr);
    datagram.setDestination(QHostAddress::LocalHost, 1489);

    _socket.writeDatagram(datagram);
}



bool Client::logIn(const QString& login, const quint16& port)
{
    if ( !_conn.connectToHost(QHostAddress("127.0.0.1"), 1488) )
        return false;

    qDebug() << login;
    qDebug() << port;

    LoginMessage msg(login, port);
    auto respond = _conn.sendMsg(&msg);
    if ( respond == nullptr || respond->getType() != MessageType::LogInRespondMsg )
    {
        qDebug("shitty respond");
        return false;
    }


    _sessionId = QUuid(respond->getInfo()["SessionId"].toString());
    delete respond;
    return true;
}
