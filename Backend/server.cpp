#include "server.h"

Server::Server(QObject *parent) : QObject(parent)
{
    _server.bind(QHostAddress::LocalHost, 789);

    qDebug() << "this is server";
    connect(&_server, SIGNAL(readyRead()),
            this, SLOT(readPendingDatagrams()));
}


void Server::readPendingDatagrams()
{
    qDebug() << "there's something to do";


    while (_server.hasPendingDatagrams())
    {
        auto datagram = _server.receiveDatagram();
        auto arr = datagram.data();

        auto x = *reinterpret_cast<const float*>(arr.left(4).constData());
        auto y = *reinterpret_cast<const float*>(arr.mid(4, 4).constData());

        emit datagramReceived(x, y);
    }
}
