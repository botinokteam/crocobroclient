#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QUdpSocket>
#include <QNetworkDatagram>
#include <QHostAddress>

class Server : public QObject
{
    Q_OBJECT

    QUdpSocket _server;

public:
    explicit Server(QObject *parent = nullptr);

signals:
    void datagramReceived(const float& x, const float& y);

public slots:
    void readPendingDatagrams();

};

#endif // SERVER_H
