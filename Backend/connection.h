#ifndef CONNECTION_H
#define CONNECTION_H

#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include "Messages/message.h"



class Connection
{

private:
    QTcpSocket _tcpSocket;


public:
    Connection();

    bool connectToHost(const QHostAddress& address, const quint16& port);
    Message* sendMsg(const Message* const msg);
};

#endif // CONNECTION_H
