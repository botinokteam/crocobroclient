#include "connection.h"



Connection::Connection()
{
}



bool Connection::connectToHost(const QHostAddress& address, const quint16& port)
{
    _tcpSocket.connectToHost(address, port);
    if ( !_tcpSocket.waitForConnected(5000) )
    {
        qDebug() << "some socket error: " + _tcpSocket.errorString();
        return false;
    }

    qDebug("tcp connection good");
    return true;
}



Message* Connection::sendMsg(const Message* const msg)
{
    _tcpSocket.write(msg->serilize());
    if ( !_tcpSocket.waitForBytesWritten(5000) )
    {
        qDebug("written");
        return nullptr;
    }

    if ( !_tcpSocket.waitForReadyRead(5000) )
    {
        qDebug("there is no answer");
        return nullptr;
    }


    auto respond = _tcpSocket.readAll();
    return Message::tryReadMsg(respond);
}
