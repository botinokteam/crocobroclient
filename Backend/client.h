#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QUdpSocket>
#include <QNetworkDatagram>
#include <QHostAddress>
#include <QTcpSocket>
#include <QUuid>
#include "connection.h"
#include "Messages/loginmessage.h"


class Client : public QObject
{
    Q_OBJECT


private:
    Connection _conn;
    QUdpSocket _socket;

    QUuid _sessionId;


public:
    explicit Client(QObject *parent = nullptr);

    Q_INVOKABLE void sendDatagram(const float& x, const float& y);
    Q_INVOKABLE bool logIn(const QString& login, const quint16& port);

signals:

public slots:


};

#endif // CLIENT_H
