#ifndef MESSAGE_H
#define MESSAGE_H

#include <QSet>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include "utility.h"



enum MessageType
{
    None = -1,
    LogInMsg,
    LogInRespondMsg
};



class Message
{

protected:
    MessageType _type;

protected:
    QJsonObject createBase() const;
    QByteArray getMsgBytes(const QJsonObject& obj) const;


public:
    Message(const MessageType& type = MessageType::None);

    MessageType getType() const;
    virtual QByteArray serilize() const = 0;
    virtual QJsonObject getInfo() const = 0;

    virtual ~Message();

    static Message* tryReadMsg(const QByteArray& bytes);
    static MessageType getType(const int& type);
};

#endif // MESSAGE_H
