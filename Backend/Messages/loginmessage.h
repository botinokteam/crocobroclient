#ifndef LOGINMESSAGE_H
#define LOGINMESSAGE_H

#include "message.h"


class LoginMessage : public Message
{

private:
    QString _login;
    quint16 _port;

    // respond fields
    QString _sessionId;


public:
    LoginMessage(const QString& login, const quint16& port);
    LoginMessage(const QString& sessionId);

    QByteArray serilize() const;
    QJsonObject getInfo() const;

    static LoginMessage* newMsg(const QJsonObject& jobj);
};

#endif // LOGINMESSAGE_H
