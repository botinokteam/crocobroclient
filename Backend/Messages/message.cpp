#include "message.h"
#include "loginmessage.h"



Message::Message(const MessageType& type) : _type(type)
{
}



QJsonObject Message::createBase() const
{
    QJsonObject obj;
    obj.insert("MsgType", _type);
    return obj;
}



QByteArray Message::getMsgBytes(const QJsonObject& obj) const
{
    auto res = QJsonDocument(obj).toBinaryData();

    auto len = res.length();
    res.prepend(reinterpret_cast<const char*>(&len), 4);

    return res;
}



MessageType Message::getType() const
{
    return _type;
}




Message* Message::tryReadMsg(const QByteArray& bytes)
{
    auto doc = QJsonDocument::fromJson(bytes.mid(MSG_LEN_BYTES_NUM));
    if ( doc.isNull() )
        return nullptr;

    auto obj = doc.object();
    auto value = obj["MsgType"].toInt(-1);
    auto type = (MessageType)value;


    if ( type == MessageType::LogInRespondMsg )
        return LoginMessage::newMsg(obj);
    else
        return nullptr;
}



MessageType Message::getType(const int& type)
{
    static QSet<MessageType> typesSet;

    if ( !typesSet.empty() )
        return typesSet.contains((MessageType)type) ?
                    (MessageType)type : MessageType::None;


    typesSet.insert(MessageType::LogInMsg);
    typesSet.insert(MessageType::LogInRespondMsg);


    return typesSet.contains((MessageType)type) ?
                (MessageType)type : MessageType::None;
}



Message::~Message()
{
}
