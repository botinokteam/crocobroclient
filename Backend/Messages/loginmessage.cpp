#include "loginmessage.h"



LoginMessage::LoginMessage(const QString& login, const quint16& port) :
    Message(MessageType::LogInMsg), _login(login), _port(port)
{
}

LoginMessage::LoginMessage(const QString& sessionId) :
    Message(MessageType::LogInRespondMsg), _sessionId(sessionId)
{
}



QByteArray LoginMessage::serilize() const
{
    if ( _type != MessageType::LogInMsg )
        return QByteArray();


    auto res = Message::createBase();
    res.insert("Login", _login);
    res.insert("Port", _port);

    return Message::getMsgBytes(res);
}



QJsonObject LoginMessage::getInfo() const
{
    if ( _type != MessageType::LogInRespondMsg )
        return QJsonObject();

    QJsonObject res;
    res.insert("SessionId", _sessionId);
    return res;
}



LoginMessage* LoginMessage::newMsg(const QJsonObject& jobj)
{
    auto sessionId = jobj["SessionId"];
    if ( sessionId.isUndefined() )
        return nullptr;

    return new LoginMessage(sessionId.toString());
}
